import importlib
import os

import torch

import dataloader.dataloaders as data_module
import model.loss as loss_module
import model.metric as metrics_module
import model.model as model_module
from utils import load_trained_state_dict


class Experiment:
    def __init__(self, rank, config: dict):
        # Configuration
        self.config = config
        self.rank = rank
        # Data Loading
        self.dataloader, self.validation_dataloader = self._get_dataloader()
        # Model and Criterion
        self.model = self._get_model()
        self.loss_fn = self._get_lossfn()
        self.optimizer = self._get_optimizer()
        self.scheduler = self._get_scheduler()
        # Metrics
        self.metrics = self._get_metrics()

    def _get_dataloader(self):
        dataloader_config = self.config['dataloader']['args']
        # Add additional configurations for the dataloader here
        dataloader_config.update({"rank": self.rank})

        # Instantiate dataloader and create validation set dataloader
        dataloader = getattr(data_module, self.config['dataloader']['type'])(**dataloader_config)
        validation_dataloader = dataloader.split_validation()

        return dataloader, validation_dataloader

    def _get_model(self):
        # If model structure is conditioned on a dataset value, update the config
        model_config = self.config['model']['args']
        # i.e model_config.update({"iheight": self.dataloader.dataset.img_height})

        # Instantiate given model
        model = getattr(model_module, self.config['model']['type'])(**model_config)

        # Load trained weights if provided
        if self.config['trained_weights'] is not None:
            if not os.path.exists(self.config['trained_weights']):
                print("The trained weights file does not exist! Check your config file!")
                return -1
            try:
                load_trained_state_dict(model, self.config['trained_weights'])
            except AttributeError:
                print("The trained weights have keys that do not exist in the model,"
                      " check your model and trained weights configuration!")
                return -1

        return model

    def _get_lossfn(self):
        return getattr(loss_module, self.config['loss'])

    def _get_optimizer(self):
        opt_config = self.config['optimizer']
        trainable_params = filter(lambda p: p.requires_grad, self.model.parameters())
        if 'module' not in opt_config.keys():
            module = torch.optim
        else:
            module = importlib.import_module(opt_config['module'])
        optimizer_config = self.config['optimizer']['args']
        return getattr(module, opt_config['type'])(trainable_params, **optimizer_config)

    def _get_scheduler(self):
        lr_type = self.config['lr_scheduler']['type']
        lr_config = self.config['lr_scheduler']['args']
        return getattr(torch.optim.lr_scheduler, lr_type)(self.optimizer, **lr_config)

    def _get_metrics(self):
        return [getattr(metrics_module, met) for met in self.config['metrics']]
