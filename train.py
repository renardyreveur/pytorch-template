import argparse
from pathlib import Path

import numpy as np
import torch
import torch.distributed as dist
import torch.multiprocessing as mp

from build_experiment.experiment import Experiment
from build_experiment.tracker import Tracker
from logger import setup_logging
from parse_config import parse_config, create_save_directory
from trainer import Trainer
from utils import init_distributed_process

# Fix random seed for reproducibility
SEED = 123
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
np.random.seed(SEED)


def main(rank, config):
    # Set up distributed processes that all look towards the same Primary Address
    if config['distributed']:
        init_distributed_process(rank, config['n_gpu'])
        print(f"Rank {rank + 1}/{config['n_gpu']} process initialized.\n")

    # Set up Experiment (Model, Data, Criterion, Optimizer, etc.)
    experiment = Experiment(rank, config)

    # Set up Experiment Tracking (Rank 0 only)
    tracker = Tracker(rank, config, experiment)

    # Set up Logging (Rank 0 only)
    if rank == 0:
        create_save_directory(config)
        setup_logging(Path(config['log_dir']))

    # Instantiate Trainer
    trainer = Trainer(rank=rank, config=config,
                      experiment=experiment, tracker=tracker)

    # Sync up before training
    if config['distributed']:
        dist.barrier()

    # Start Training!
    trainer.train()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='PyTorch Template')
    parser.add_argument('-c', '--config', default=None, type=str,
                        help='config file path (default: None)')
    parser.add_argument('-d', '--device', default=None, type=str,
                        help='indices of GPUs to enable (default: all, e.g "2,3")')
    args = parser.parse_args()

    # Parse configuration file
    cfg = parse_config(args)

    # Start main process / distributed processes
    if cfg['distributed']:
        mp.spawn(main, args=(cfg,), nprocs=cfg['n_gpu'], join=True)
    else:
        main(0, cfg)
