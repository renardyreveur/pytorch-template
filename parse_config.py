import os
from datetime import datetime
from pathlib import Path

import torch
import yaml


# Initialization function
def parse_config(args):
    # Read YAML file as dictionary
    with open(Path(args.config), mode='rt') as cfile:
        config = yaml.safe_load(cfile)

    # Set number of visible GPUs
    if args.device is not None:
        detected_gpus = torch.cuda.device_count()
        try:
            gpu_indices = [int(x) for x in args.device.split(",")]
            if len(gpu_indices) != config['n_gpu'] or max(gpu_indices) > detected_gpus - 1:
                raise ValueError
            os.environ["CUDA_VISIBLE_DEVICES"] = args.device
        except ValueError:  # Either max index above total gpu count, or indices not integer
            print(f"Please check the device indices [args.device], continuing with detected GPUs")
            config['n_gpu'] = detected_gpus

    # Check distributed condition
    if config['distributed'] and config['n_gpu'] < 2:
        print("Cannot run distributed processes when n_gpu < 2, training non-distributed mode")
        config['distributed'] = False

    return config


def create_save_directory(config):
    # Get run_id from experiment tracking details, if not use current datetime
    experiment_name = config['name']
    run_id = config["track_experiment"]["config"]["run_id"] if config["track_experiment"]["track"]\
        else datetime.now().strftime(r'%m%d_%H%M%S')

    # Set save directory where the trained models and logs will be
    save_dir = Path(config['trainer']['save_dir'])
    model_dir = save_dir / 'models' / experiment_name / run_id
    log_dir = save_dir / 'log' / experiment_name / run_id

    # Create directory for saving weight checkpoints and log.
    exist_ok = True  # run_id == ''
    model_dir.mkdir(parents=True, exist_ok=exist_ok)
    log_dir.mkdir(parents=True, exist_ok=exist_ok)

    # Save config file to the checkpoint dir
    with open(Path(model_dir / 'config.yaml'), 'w') as file:
        yaml.dump(config, file, indent=4, default_flow_style=False)
    config.update({"save_dir": str(model_dir), "log_dir": str(log_dir)})
