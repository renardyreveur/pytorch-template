import logging
import time

import torch
from numpy import inf
from torch.nn.parallel import DistributedDataParallel


class BaseTrainer:
    def __init__(self, rank, config, experiment):
        self.config = config

        # Only initialise the logger on first process
        if rank == 0:
            self.logger = logging.getLogger('trainer')

        # Distributed Training
        self.rank, self.world_size = rank, config['n_gpu']

        # Setup GPU device if available, move model into configured device
        self.device, device_ids = self._prepare_device(config['n_gpu'], self.rank)

        # Distributed Data Parallel / Standard Data Parallel
        if config['distributed']:
            self.model = experiment.model.cuda(rank)
            self.model = DistributedDataParallel(self.model, device_ids=[rank], find_unused_parameters=True)
        else:
            self.model = experiment.model.to(self.device)
            if len(device_ids) > 1:
                self.model = torch.nn.DataParallel(experiment.model, device_ids=device_ids)

        self.criterion = experiment.loss_fn
        self.metric_ftns = experiment.metrics
        self.optimizer = experiment.optimizer
        self.lr_scheduler = experiment.scheduler

        cfg_trainer = config['trainer']
        self.epochs = cfg_trainer['epochs']
        self.save_period = cfg_trainer['save_period']
        self.monitor = cfg_trainer.get('monitor', 'off')

        # Configuration to monitor model performance and save best
        if self.monitor == 'off':
            self.mnt_mode = 'off'
            self.mnt_best = 0
        else:
            self.mnt_mode, self.mnt_metric = self.monitor.split()
            assert self.mnt_mode in ['min', 'max']

            self.mnt_best = inf if self.mnt_mode == 'min' else -inf
            self.early_stop = cfg_trainer.get('early_stop', inf)

        self.start_epoch = 1

        self.checkpoint_dir = config['save_dir']

        # dist.barrier()

    # Implemented in trainer/trainer.py
    def _train_epoch(self, epoch):
        """
        Training logic for an epoch

        :param epoch: Current epoch number
        """
        raise NotImplementedError

    def train(self):
        """
        Full training logic
        """
        not_improved_count = 0
        for epoch in range(self.start_epoch, self.epochs + 1):
            start_epoch = time.time()
            result = self._train_epoch(epoch)
            end_epoch = time.time()
            epoch_duration = end_epoch - start_epoch

            # Save logged information into log dict
            log = None
            if self.rank == 0:
                log = {'epoch': epoch, 'duration': epoch_duration}
                log.update(result)

                # Print logged information to the screen
                for key, value in log.items():
                    if self.world_size > 1:
                        print('    {:15s}: {}'.format(str(key), value))
                    else:
                        self.logger.info('    {:15s}: {}'.format(str(key), value))
            # dist.barrier()

            # Evaluate model performance according to configured metric, and save the best checkpoint as model_best
            best = False
            if self.mnt_mode != 'off' and self.rank == 0:
                try:
                    # Check whether model performance improved or not, according to specified metric(mnt_metric)
                    improved = (self.mnt_mode == 'min' and log[self.mnt_metric] <= self.mnt_best) or \
                               (self.mnt_mode == 'max' and log[self.mnt_metric] >= self.mnt_best)
                except KeyError:
                    self.logger.warning("Warning: Metric '{}' is not found. "
                                        "Model performance monitoring is disabled.".format(self.mnt_metric))
                    self.mnt_mode = 'off'
                    improved = False

                if improved:
                    self.mnt_best = log[self.mnt_metric]
                    not_improved_count = 0
                    best = True
                else:
                    not_improved_count += 1

                if not_improved_count > self.early_stop:
                    if self.rank == 0:
                        self.logger.info("Validation performance didn\'t improve for {} epochs. "
                                         "Training stops.".format(self.early_stop))
                    break

            if self.rank == 0 and epoch % self.save_period == 0:
                self._save_checkpoint(epoch, save_best=best)
            # dist.barrier()

    def _prepare_device(self, n_gpu_use, rank):
        """
        Setup GPU device if available, move model into configured device
        """
        n_gpu = torch.cuda.device_count()

        if n_gpu_use > 0 and n_gpu == 0:
            self.logger.warning("Warning: There\'s no GPU available on this machine,"
                                "training will be performed on CPU.")
        if n_gpu_use > n_gpu:
            self.logger.warning(f"Warning: The number of GPU\'s configured to use is {n_gpu_use}, but only {n_gpu}"
                                f" are available on this machine.")

        device = torch.device(f'cuda:{rank}' if n_gpu_use > 0 else 'cpu')

        # Check for Mac M1 Metal Performance Shaders
        if torch.backends.mps.is_available() and n_gpu_use > 0:
            self.logger.warning("Using M1 Metal Performance Shaders(MPS) for Hardware Acceleration!")
            device = torch.device("mps")

        list_ids = list(range(n_gpu))
        return device, list_ids

    def _save_checkpoint(self, epoch, save_best=False):
        """
        :param epoch: current epoch number
        :param save_best: if True, rename the saved checkpoint to 'model_best.pth'
        """
        arch = type(self.model).__name__
        state = {
            'arch': arch,
            'epoch': epoch,
            'state_dict': self.model.state_dict(),
            'optimizer': self.optimizer.state_dict(),
            'lr_scheduler': self.lr_scheduler.state_dict(),
            'monitor_best': self.mnt_best,
            'config': self.config.config
        }
        filename = str(self.checkpoint_dir / 'checkpoint-epoch{}.pth'.format(epoch))
        torch.save(state, filename)
        self.logger.info("Saving checkpoint: {} ...".format(filename))
        if save_best:
            best_path = str(self.checkpoint_dir / 'model_best.pth')
            torch.save(state, best_path)
            self.logger.info("Saving current best: model_best.pth ...")
