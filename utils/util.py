import collections
import os
from collections import OrderedDict
from functools import reduce
from json import loads, dumps
from pathlib import Path

import pandas as pd
import torch
import torch.distributed as dist


# Class that defines and keeps track of metric functions
class MetricTracker:
    def __init__(self, *keys):
        self._data = pd.DataFrame(index=keys, columns=['total', 'counts', 'average'])
        self.reset()

    def reset(self):
        for col in self._data.columns:
            self._data[col].values[:] = 0

    def update(self, key, value, n=1):
        self._data.total[key] += value * n
        self._data.counts[key] += n
        self._data.average[key] = self._data.total[key] / self._data.counts[key]

    def avg(self, key):
        return self._data.average[key]

    def result(self):
        return dict(self._data.average)


# Function that loads weights to a model
def load_trained_state_dict(model, trained_dict):
    trained_weights = torch.load(trained_dict)

    # If state_dict was saved from DataParallel Model, change state_dict
    if list(trained_weights.keys())[0][:6] == "module":
        new_state_dict = OrderedDict()
        for k, v in trained_weights.items():
            name = k[7:]
            new_state_dict[name] = v
        trained_weights = new_state_dict

    # Iterate over the items of the weights to update
    for k, v in trained_weights.items():
        module = reduce(getattr, [model] + k.split("."))
        module.data = v


# Utility Functions

# Loop the iterator into an infinite generator
# def inf_loop(dataloader):
#     """ wrapper function for endless data loader. """
#     for loader in repeat(dataloader):
#         yield from loader


def ensure_dir(dirname):
    dirname = Path(dirname)
    if not dirname.is_dir():
        dirname.mkdir(parents=True, exist_ok=False)


def to_dict(input_ordered_dict):
    return loads(dumps(input_ordered_dict))


def flatten(d, parent_key='', sep='_'):
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.abc.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']


def init_distributed_process(rank, world_size, backend='nccl'):
    """ Initialize the distributed environment. """
    os.environ['MASTER_ADDR'] = '127.0.0.1'
    os.environ['MASTER_PORT'] = '29500'
    dist.init_process_group(backend, rank=rank, world_size=world_size)
